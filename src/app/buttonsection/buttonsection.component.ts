import { Component, EventEmitter, OnInit,Output } from '@angular/core';

@Component({
  selector: 'app-buttonsection',
  templateUrl: './buttonsection.component.html',
  styleUrls: ['./buttonsection.component.scss']
})
export class ButtonsectionComponent implements OnInit {

  @Output() home: EventEmitter<string> = new EventEmitter<string>();
  @Output() about: EventEmitter<string> = new EventEmitter<string>();
  @Output() service: EventEmitter<string> = new EventEmitter<string>();
  @Output() contact: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }

  sayHome($event : any) {
    this.home.emit('Hello Home button , You have clicked the home button ');
  }

  sayAbout($event : any) {
    this.about.emit('Hello About button , You have clicked the about button ');
  
  }

  sayService($event : any) {
    this.service.emit('Hello service button , You have clicked the Services button ');
  
  }

  sayContact($event : any) {
    this.contact.emit('Hello Contact button , You have clicked the Contact button ');
  
  }



}
