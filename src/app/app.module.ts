import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ButtonsectionComponent } from './buttonsection/buttonsection.component';


@NgModule({
  declarations: [
    AppComponent,
    ButtonsectionComponent,
  
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
